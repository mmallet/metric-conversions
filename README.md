The metrical app is a simple to convert Metric units to Imperial and vise versa. Metrical was built in Java Spring Boot running 

Spring 2 and Maven for the back-end part. The front end was building on Angular 6 in Typescript running on a Nginx container. 

This application is reliant on the front-end to pull the data. It can also be found in this repo. 


**GETTING STARTED**

To run the metrical app you can either pull the code from this repo, or pull the images from Dockerhub and run it in a

container locally.



**PULLING FROM DOCKER**

docker pull marcmallet/metric-conversion-api

and then

docker pull marcmallet/metric-conversion-client



**RUNNING FROM DOCKER**

To run the application, simply run this comand in your console/terminal:

docker run --rm -it -p 8080:8080 marcmallet/metric-conversion-api:latest

and then

docker run --rm -it -p 80:80 marcmallet/metric-conversion-client:latest

Then open your browser and visit http://localhost:80/



**RUNNING FROM LOCAL (AFTER PULLED FROM THIS REPO)**

Download or pull this repo to your local machine and then CD into the 'client' DIR and run in your console/terminal:

npm install

npm start || ng serve --open

(Ensure that your browser is ipen and pointing to this URL: http://localhost:4200)