import { Component, OnInit } from '@angular/core';
import { MetricService } from './services/metric.service';
import { FormGroup, FormControl } from '@angular/forms';

//Set component prequisites
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  //Set component variables
  title='Metric Convertor'; 
  message='This is a simple metric conversion app. You can convert 3 types: Distance, Mass and Temperature. The conversion work vise versa. Give it a go!';
  showcase=''; 
  suffix='';
  active='';
  metricform:FormGroup; 

  constructor(private metricService: MetricService){ }
    ngOnInit(){

      //Define form fields
      this.metricform = new FormGroup({
        'frmMiles': new FormControl(),
        'frmKilometers': new FormControl(),
        'frmPounds': new FormControl(),
        'frmKilograms': new FormControl(),
        'frmFahrenheit': new FormControl(),
        'frmCelsius': new FormControl()
      });


  }


  //function for the selected form field. this clears all unuse fields
  setStateFocus(tag){ 
    this.active = 'frm'+tag;

    if(this.active == 'frmMiles')
    { this.metricform.controls['frmMiles'].setValue('');

      this.metricform.controls['frmPounds'].setValue('');
      this.metricform.controls['frmKilograms'].setValue('');
      this.metricform.controls['frmFahrenheit'].setValue('');
      this.metricform.controls['frmCelsius'].setValue('');
    }
    else if(this.active == 'frmKilometers')
    { this.metricform.controls['frmKilometers'].setValue('');

    this.metricform.controls['frmPounds'].setValue('');
    this.metricform.controls['frmKilograms'].setValue('');
    this.metricform.controls['frmFahrenheit'].setValue('');
    this.metricform.controls['frmCelsius'].setValue('');
    }




    else if(this.active == 'frmPounds')
    { this.metricform.controls['frmPounds'].setValue('');

    this.metricform.controls['frmMiles'].setValue('');
    this.metricform.controls['frmKilometers'].setValue('');
    this.metricform.controls['frmFahrenheit'].setValue('');
    this.metricform.controls['frmCelsius'].setValue('');
    }
    else if(this.active == 'frmKilograms')
    { this.metricform.controls['frmKilograms'].setValue('');

    this.metricform.controls['frmMiles'].setValue('');
    this.metricform.controls['frmKilometers'].setValue('');
    this.metricform.controls['frmFahrenheit'].setValue('');
    this.metricform.controls['frmCelsius'].setValue('');
    }




    else if(this.active == 'frmFahrenheit')
    { this.metricform.controls['frmFahrenheit'].setValue('');

    this.metricform.controls['frmMiles'].setValue('');
    this.metricform.controls['frmKilometers'].setValue('');
    this.metricform.controls['frmPounds'].setValue('');
    this.metricform.controls['frmKilograms'].setValue('');
    }
    else if(this.active == 'frmCelsius')
    { this.metricform.controls['frmCelsius'].setValue('');

    this.metricform.controls['frmMiles'].setValue('');
    this.metricform.controls['frmKilometers'].setValue('');
    this.metricform.controls['frmPounds'].setValue('');
    this.metricform.controls['frmKilograms'].setValue('');
    }

  }





  //This is the onkeyup function that gets called to grab the value pass it through to the method that handles api requests
  sendMetric(tag){ 

    //CLEAR FIELDS
    if((tag == 'Miles')||(tag == 'Kilometers'))
    {
      this.metricform.controls['frmPounds'].setValue('');
      this.metricform.controls['frmKilograms'].setValue('');
      this.metricform.controls['frmFahrenheit'].setValue('');
      this.metricform.controls['frmCelsius'].setValue('');
    }
    else if((tag == 'Pounds')||(tag == 'Kilograms'))
    {
      this.metricform.controls['frmMiles'].setValue('');
      this.metricform.controls['frmKilometers'].setValue('');
      this.metricform.controls['frmFahrenheit'].setValue('');
      this.metricform.controls['frmCelsius'].setValue('');
    }
    else if((tag == 'Fahrenheit')||(tag == 'Celsius'))
    {
      this.metricform.controls['frmMiles'].setValue('');
      this.metricform.controls['frmKilometers'].setValue('');
      this.metricform.controls['frmPounds'].setValue('');
      this.metricform.controls['frmKilograms'].setValue('');
    }



    //set the suffix that will get return along with the api value
    if(tag == 'Miles')
    { this.suffix = ' kms';
    }
    else if(tag == 'Kilometers')
    { this.suffix = ' mi';
    }
    else if(tag == 'Pounds')
    { this.suffix = ' kg';
    }
    else if(tag == 'Kilograms')
    { this.suffix = ' lb';
    }
    else if(tag == 'Fahrenheit')
    { this.suffix = ' °C';
    }
    else if(tag == 'Celsius')
    { this.suffix = ' °F';
    }
    else
    { this.suffix = '';
    }


    //SET VARIABLES
    let stag = 'frm'+tag;
    const postdata = {
      key: tag,
      value: this.metricform.get(stag).value
    };
   
    //MAKE API CALL
    this.metricService.setMetrics(postdata).subscribe(
      data => {
        this.metricform.controls['frm'+data['key']].setValue(data['value']);
        this.showcase = data['value'] + this.suffix;
     });
    
   }
  



}
