import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type':'application/json'})
};

@Injectable({  providedIn: 'root'})
export class MetricService {

  constructor(private http:HttpClient) { }
  
  //default function that queries api and returns results
  setMetrics(metric){
    let body = JSON.stringify(metric);
    return this.http.post('http://localhost:8080/', body, httpOptions);
    
  }

}
