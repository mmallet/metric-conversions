package com.marcmallet.mmallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmalletApplication.class, args);
	}
}
