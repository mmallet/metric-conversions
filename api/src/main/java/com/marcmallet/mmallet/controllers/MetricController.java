package com.marcmallet.mmallet.controllers;

import com.marcmallet.mmallet.models.Metric;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class MetricController {


    @RequestMapping(value = "/")
    public ResponseEntity<Metric> get(){

        Metric metric = new Metric();
        metric.setKey("Pounds");
        metric.setValue(100);

        return new ResponseEntity<Metric>(metric, HttpStatus.OK);
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Metric> Update(@RequestBody Metric metric){

        String tmp_key = metric.getKey();
        double tmp_value = metric.getValue();

        if(metric != null) {


            if(tmp_key.equals("Miles")) {
                double x = 1.6;
                metric.setValue(Math.round(tmp_value * x));
                metric.setKey("Kilometers");
            }
            else if(tmp_key.equals("Kilometers")) {
                double x = 1.6;
                metric.setValue(Math.round(tmp_value / x));
                metric.setKey("Miles");
            }



            else if (tmp_key.equals("Kilograms")) {
                double x = 2.20462;
                metric.setValue(Math.round(tmp_value * x));
                metric.setKey("Pounds");
            }
            else if(tmp_key.equals("Pounds")) {
                double x = 2.20462;
                metric.setValue(Math.round(tmp_value / x));
                metric.setKey("Kilograms");
            }



            else if(tmp_key.equals("Celsius")) {
                double x = 1.8;
                metric.setValue(Math.round(((tmp_value * x)+32)));
                metric.setKey("Fahrenheit");
            }
            else if(tmp_key.equals("Fahrenheit")) {
                double x =  0.5556;
                metric.setValue(Math.round(((tmp_value - 32) * x)));
                metric.setKey("Celsius");
            }

        }

        return new ResponseEntity<Metric>(metric, HttpStatus.OK);
    }

}
