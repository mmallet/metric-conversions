package com.marcmallet.mmallet;


import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;



@RunWith(SpringRunner.class)
@SpringBootTest
public class MmalletApplicationTests {
	/*
	@Before
	public void before(){
		System.out.println("Before");
	}
	@After
	public void after(){
		System.out.println("After");
	}
*/


	@Test
	public void testAssertEquals() {
		assertEquals("Ruben, this failure - strings are not equal", "text", "text");
		System.out.println("Ruben, this failure - strings are not equal");
	}
	@Test
	public void testAssertFalse() {
		assertFalse("Ruben, this failure - should be false", false);
		System.out.println("Ruben, this failure - should be false");
	}
	@Test
	public void testAssertNotNull() {
		assertNotNull("Ruben, this should not be null", new Object());
		System.out.println("Ruben, this should not be null");
	}
	@Test
	public void testAssertNotSame() {
		assertNotSame("Ruben, this should not be same Object", new Object(), new Object());
		System.out.println("Ruben, this should not be same Object");
	}

	@Test
	public void testAssertThatHasItems() {
		assertThat(Arrays.asList("one", "two", "three"), hasItems("one", "three"));
	}
	@Test
	public void testAssertThatEveryItemContainsString() {
		assertThat(Arrays.asList(new String[] { "fun", "ban", "net" }), everyItem(containsString("n")));
	}

}
